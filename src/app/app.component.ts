import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){
    console.log("App Componenent call");
  }


  //First 
  //title = 'Hello Angular';

  //Second  
  
  // xPosition = 0.0;
  // yPosition = 0.0;
  // mousemoveEvent(event){
  //   this.xPosition = event.clientX;
  //   this.yPosition = event.clientY;
  // }

  //Third

  // loginSuccess = false;
  // loginError = false;
  // customClass = "";
  // data = { 
  //   vEmail: "",
  //   vPassword: ""
  // };
  // clickDemo(data){
  //   console.log(data);
  //   if(data.vEmail == 'admin@admin.com' && data.vPassword == 'admin'){
  //     this.loginSuccess = true;
  //     this.loginError = false;
  //     this.customClass = "green";
  //   }else{
  //     this.loginError = true;
  //     this.loginSuccess = false;
  //     this.customClass = "red";
  //   }
  // }
  
  //Four
  // size = 20;
  

  //Five 
  // data = [
  //   {name:"ashish",email:"ashish@gmail.com"},
  //   {name:"harsh",email:"harsh@gmail.com"},
  //   {name:"chetan",email:"chetan@gmail.com"},
  //   {name:"milan",email:"milan@gmail.com"},
  // ]; 

  //Six 
  status = false;
  

  
}